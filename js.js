// JS dark mode for page
function mode() {
  document.querySelector(".sun-mode").style.display = "block";
  document.querySelector(".moon-mode").style.display = "none";
  document.body.classList.toggle("dark");
}
function modeLight() {
  document.querySelector(".sun-mode").style.display = "none";
  document.querySelector(".moon-mode").style.display = "block";
  document.body.classList.toggle("dark");
}
// -----open overlay header menu----------

// -----------------------------------------------
// count number middle
$(document).ready(function () {
  $(".counter").each(function () {
    var count = $(this);
    var countTo = count.attr("data-count");
    // console.log(countTo);
    $({ countNum: count.text() }).animate(
      {
        countNum: countTo,
      },
      {
        duration: 5000,
        easing: "linear",
        step: function () {
          count.text(Math.floor(this.countNum));
        },
        complete: function () {
          count.text(this.countNum);
        },
      }
    );
  });
});
// -------------------------slick;
$(document).ready(function () {
  $(".list-testimonials").slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
  });
});
// ------------------------
